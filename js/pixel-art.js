  window.onload = function(){
    recorridoColores();
    creandoGrilla();
  }

  var nombreColores = ['White', 'LightYellow',
  'LemonChiffon', 'LightGoldenrodYellow', 'PapayaWhip', 'Moccasin', 'PeachPuff', 'PaleGoldenrod', 'Bisque', 'NavajoWhite', 'Wheat', 'BurlyWood', 'Tan',
  'Khaki', 'Yellow', 'Gold', 'Orange', 'DarkOrange', 'OrangeRed', 'Tomato', 'Coral', 'DarkSalmon', 'LightSalmon', 'LightCoral', 'Salmon', 'PaleVioletRed',
  'Pink', 'LightPink', 'HotPink', 'DeepPink', 'MediumVioletRed', 'Crimson', 'Red', 'FireBrick', 'DarkRed', 'Maroon',
  'Brown', 'Sienna', 'SaddleBrown', 'IndianRed', 'RosyBrown',
  'SandyBrown', 'Goldenrod', 'DarkGoldenrod', 'Peru',
  'Chocolate', 'DarkKhaki', 'DarkSeaGreen', 'MediumAquaMarine',
  'MediumSeaGreen', 'SeaGreen', 'ForestGreen', 'Green', 'DarkGreen', 'OliveDrab', 'Olive', 'DarkOliveGreen', 'YellowGreen', 'LawnGreen',
  'Chartreuse', 'GreenYellow', 'Lime', 'SpringGreen', 'LimeGreen',
  'LightGreen', 'PaleGreen', 'PaleTurquoise',
  'AquaMarine', 'Cyan', 'Turquoise', 'MediumTurquoise', 'DarkTurquoise', 'DeepSkyBlue',
  'LightSeaGreen', 'CadetBlue', 'DarkCyan', 'Teal', 'Steelblue', 'LightSteelBlue', 'Honeydew', 'LightCyan',
  'PowderBlue', 'LightBlue', 'SkyBlue', 'LightSkyBlue',
  'DodgerBlue', 'CornflowerBlue', 'RoyalBlue', 'SlateBlue',
  'MediumSlateBlue', 'DarkSlateBlue', 'Indigo', 'Purple', 'DarkMagenta', 'Blue',
  'MediumBlue', 'DarkBlue', 'Navy', 'Thistle',
  'Plum', 'Violet', 'Orchid', 'DarkOrchid', 'Fuchsia', 'Magenta', 'MediumOrchid',
  'BlueViolet', 'DarkViolet', 'DarkOrchid',
  'MediumPurple', 'Lavender', 'Gainsboro', 'LightGray', 'Silver', 'DarkGray', 'Gray',
  'DimGray', 'LightSlateGray', 'DarkSlateGray', 'Black'
  ];



  // Variable para guardar el elemento 'color-personalizado'
  // Es decir, el que se elige con la rueda de color.
  var colorPersonalizado = document.getElementById('color-personalizado');
  //Variable para guardar los colores que se van a mostrar en la paleta
  var idPaleta = document.getElementById('paleta');
  //Para luego poder mostrar la grilla de pixeles para pintar
  var idGrillaPixeles = document.getElementById('grilla-pixeles');
  //Variable declarada pero se inicia en recorridoColores() donde siempre guardar
  //el color seleccionado, ya sea uno de la paleta o uno personalizado
  var $colorSeleccionado;

  var botonGuardar = document.getElementById('guardar');

  //recorre el arreglo nombreColores y crea la paleta de colores predeterminados
  function recorridoColores(){
    var hijoDePaleta;
    for(var i = 0; i < nombreColores.length; i++){
      hijoDePaleta = document.createElement('div');
      hijoDePaleta.className = "color-paleta";
      hijoDePaleta.style.backgroundColor = nombreColores[i];
      idPaleta.appendChild(hijoDePaleta);
    }

    $colorSeleccionado = $("#paleta div.color-paleta");
    $colorSeleccionado.click(function(){
      var color = $(this).attr("style");
      indicadorColor(color);
    });
  }


  //crea la grilla de pixeles para luego dibujar
  function creandoGrilla(){
    var arrGrilla = [];
    for(var i = 0; i < 33; i++){
      arrGrilla = new Array(33);
      for(var j = 0; j < 53; j++){
        hijoDeGrilla = document.createElement('div');
        idGrillaPixeles.appendChild(hijoDeGrilla);
      }
    }
  }

  //cambia el cuadradito que muestra el color seleccionado
  function indicadorColor(color){
    var $colorActual = $("#indicador-de-color");
    $colorActual.attr("style",color);
    cambiarColorGrilla(color);
  }

  //pinta los cuadraditos de la grilla, es decir crea el dibujo
  function cambiarColorGrilla(color){
      var $pixelPintado = $("#grilla-pixeles").find("div");
      var clickPresionado = false;
      var deteccionMouseUp = $("body");

      $pixelPintado.mousedown(function(){
        $(this).attr("style",color);
        clickPresionado = true;
      });

      deteccionMouseUp.mouseup(function(){
        clickPresionado = false;
      });

      $pixelPintado.mouseover(function(){
        if(clickPresionado){
          $(this).attr("style",color);
        }
      });

    }


  colorPersonalizado.addEventListener('change',
    (function() {
      // Se guarda el color de la rueda en colorActual
      colorActual = colorPersonalizado.value;
      var color = "background-color: " + colorActual;
      // Completar para que cambie el indicador-de-color al colorActual
      indicadorColor(color);
    })
  );

  botonGuardar.addEventListener('click',function(){
    html2canvas($("#grilla-pixeles") , {
      onrendered: function(canvas) {
        theCanvas = canvas;
        canvas.toBlob(function(blob) {
          saveAs(blob, "pixel-art.png");
        });
      }
    });
  });

  $(document).ready(function()
    {
      var $botonBorrar = $("#borrar");

      $botonBorrar.click(function(){
            var $pixelPintado = $("#grilla-pixeles").find("div");
            $pixelPintado.animate({"background-color" : "White"},900);
          });
    });



  $(document).ready(function(){
    var $superHero = $("ul li img");
    $superHero.click(function(){
      var nombre = $(this).attr("id");

      switch(nombre){

        case "batman":
          cargarSuperheroe(batman);
          break;

        case "wonder":
          cargarSuperheroe(wonder);
          break;

        case "flash":
            cargarSuperheroe(flash);
            break;

        case "invisible":
            cargarSuperheroe(invisible);
            break;
      }
    });
  });
