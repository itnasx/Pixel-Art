// No modifiques estas funciones a menos que sepas MUY BIEN lo que estas haciendo!

// Tuve que comentar la funcion guardarPixelArt() porque si solamente la usaba
// con la funcion click(), se me ejecutaba al cargar el html y salia la venta de guardar la imagen

// Abre una ventana para guardar nuestro arte en un archivo pixel-art.png
// function guardarPixelArt() {
//   console.log("hola me ejecuto sin permiso");
//   html2canvas($("#grilla-pixeles") , {
//     onrendered: function(canvas) {
//       theCanvas = canvas;
//       canvas.toBlob(function(blob) {
//         saveAs(blob, "pixel-art.png");
//       });
//     }
//   });
// }

// Carga a un superheroe predefinido
function cargarSuperheroe(superheroe) {
  var $pixeles = $("#grilla-pixeles div");
  for (var i = 0; i < superheroe.length; i++) {
    $pixeles[i].style.backgroundColor = superheroe[i];
  }
}
